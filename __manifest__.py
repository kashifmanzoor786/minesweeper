# -*- coding: utf-8 -*-
{
    'name': "Minesweeper",

    'summary': """
        Summary""",

    'description': """
        Long description
    """,

    'author': "Kashif Manzoor : Senior Odoo Developer",
    'website': "N/A",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/minesweeper_player.xml',
    ],
}