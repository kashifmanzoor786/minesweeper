from odoo import fields, models, api


class MinesweeperPlayer(models.Model):
    _name = 'minesweeper.player'
    _description = 'Minesweeper Players'

    name = fields.Many2one('res.partner')
    country_id = fields.Many2one('res.country', related="name.country_id", string="Country Name", store=True)
    image = fields.Binary(related="name.image", string="Image", store=True)
    number_of_wins = fields.Integer('Number of Wins')
    number_of_losses = fields.Integer('Number of Losses')
    avg_game_duration = fields.Char('Average Game Duration in Minutes')
    field_size = fields.Float('Field Size')
    number_of_games_field_size = fields.Float('Number of games for each field size')

